/*
Programmer: Alex Fung
Programmer's ID: 1873719
Class: COMPSC-210-5097
Date: 2021-02-22
Programming Environment Used: Linux, gcc 8.3
*/

#include <iostream>
#include <iterator> // advance, next, prev
#include <vector>
#include <chrono>

void printVector(const std::vector<int> &v) {
  for (const auto ele : v) {
    std::cout << ele << " ";
  }
  std::cout << std::endl;
}

void mergeSortRecursive(
  std::vector<int> &v, std::vector<int>::iterator beginIt, std::vector<int>::iterator endIt) {
    // Base case
    if (endIt - beginIt == 1) {
      return;
    } else {
      std::vector<int>::iterator::difference_type difference = endIt - beginIt;
      // Make a copy of iterator to make a middle iterator 
      std::vector<int>::iterator middleIt(beginIt);
      std::advance(middleIt, difference/2);
      // Divide
      mergeSortRecursive(v, beginIt, middleIt); // Sort leftHalf
      mergeSortRecursive(v, middleIt, endIt); // Sort rightHalf

      // combine 2 sorted halves
      std::vector<int>::iterator leftIt(beginIt);
      std::vector<int>::iterator rightIt(middleIt);
      std::vector<int> tempSortedValues (static_cast<size_t>(difference));
      std::vector<int>::iterator tempIt = tempSortedValues.begin();
      while (tempIt != tempSortedValues.end()) {
        // Copy the lower value (between leftHalf and righthalf) into tempSortedValues.
        if (*leftIt < *rightIt) {
          *tempIt = *leftIt;
          ++leftIt;
        } else {
          *tempIt = *rightIt;
          ++rightIt;
        }
        ++tempIt;

        if (leftIt == middleIt) {
          // Copied all values from leftHalf. No need to move remaining rightHalf to 
          // tempSortedValues since they don't have to move at all.
          tempIt = tempSortedValues.end();
        } else if (rightIt == endIt) {
          // Copied all values from rightHalf. Move the whole rest of leftHalf without 
          // checking.
          for (;leftIt != middleIt; ++leftIt) {
            *tempIt = *leftIt;
            ++tempIt;
          }
        }
      }
      // put tempSortedValues into v
      tempIt = tempSortedValues.begin();
      for (std::vector<int>::iterator it(beginIt);it != rightIt; ++it) {
        *it = *tempIt;
        ++tempIt;
      }
    }
}


void mergeSort(std::vector<int> &vec) {
  mergeSortRecursive(vec, vec.begin(), vec.end());
}

int main() {
  srand(time(0));

  std::cout << "\nAlex is a robot driver. He practices racing his robot ";
  std::cout << "in an obstacle course in his lab many times and records time required ";
  std::cout << "to complete the course. He wants to sort his times in acsending order";
  std::cout << " using merge sort." << std::endl;

  std::vector<int> seconds{3, 5, 4, 4, 1, 9, 4, 7, 10, 2, 3, 6, 8, 15};
  std::cout << "Pre-sort values: " << std::endl;
  printVector(seconds);
  mergeSort(seconds);
  std::cout << "Post-sort values: " << std::endl;
  printVector(seconds);
  
  return 0;
}
