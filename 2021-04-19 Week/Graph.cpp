/*
Programmer: Alex Fung
Programmer's ID: 1873719
Class: COMPSC-210-5097
Date: 2021-04-24
Programming Environment Used: Linux, gcc 8.3
*/

#include "Graph.h"

Graph::Graph(size_t size) : matrix(size, std::vector<int>(size, 0)) {
  if (size == 0) {
    std::cout << "Size must be larger than 0" << std::endl;
    return;
  }
}

Graph::Graph(const std::vector<std::vector<int>>& m)
  : matrix(m.size(), std::vector<int>(m.size(), 0)) {
  if (m.size() == 0) {
    std::cout << "Size must be larger than 0" << std::endl;
    return;
  }
  if (m.size() != m.at(0).size()) {
    std::cout <<  "Matrix provided must be a square" << std::endl;
    return;
  }
  for (size_t i = 0; i < m.size(); ++i) {
    for (size_t j = 0; j < m[0].size(); ++j) {
      validateEdge(m[i][j]);
    }
  }
  for (size_t i = 0; i < m.size(); ++i) {
    matrix[i] = m[i];
  }
}

void Graph::validateVertex(const size_t v) const {
  if (v > matrix.size()) {
    std::cout << "vertex " << v << " cannot be larger than graph size" << std::endl;
  }
}
void Graph::validateEdge(const int weight) const {
  if (weight < 0) {
    std::cout << "Edge weight " << weight << " cannot be less than 0" << std::endl;
  }
}
void Graph::setEdge(const size_t start, const size_t end, const int weight) {
  validateVertex(start);
  validateVertex(end);
  validateEdge(weight);
  matrix[start][end] = weight;
}
int Graph::getEdge(const size_t start, const size_t end) const {
  validateVertex(start);
  validateVertex(end);
  return matrix[start][end];
}

std::vector<std::vector<int>> Graph::getMatrix() const {
  return matrix;
}

size_t Graph::verticesCount() const {
  return matrix.size();
}

