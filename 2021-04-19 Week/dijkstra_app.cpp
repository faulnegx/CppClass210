/*
Programmer: Alex Fung
Programmer's ID: 1873719
Class: COMPSC-210-5097
Date: 2021-04-24
Programming Environment Used: Linux, gcc 8.3
*/

#include <iostream>
#include <limits>
#include <vector>

#include "Graph.h"

struct NodeForDijkstra {
  NodeForDijkstra(size_t n = std::numeric_limits<size_t>::max())
    : nodeNum(n), fromNode(std::numeric_limits<size_t>::max()),
      distance(std::numeric_limits<int>::max()), visited(false) {
  };
  size_t nodeNum;
  size_t fromNode;
  int distance;
  bool visited;
};

std::vector<NodeForDijkstra> allShortestPath(const Graph&, const size_t);
std::vector<size_t> shortestPathToEnd(const std::vector<NodeForDijkstra>&, const size_t);

// Given graph, use Dijkstra's algorithm to find the shortest paths from start to all 
// vertices.
std::vector<NodeForDijkstra> allShortestPath(const Graph& g, const size_t start) {
  std::vector<NodeForDijkstra> nodeList;
  size_t visited = 0;
  for (size_t i = 0; i < g.verticesCount(); ++i) {
    nodeList.push_back(NodeForDijkstra(i));
  }
  nodeList[start].distance = 0;


  while (visited < g.verticesCount()) {
    NodeForDijkstra currentV = nodeList[start];
    if (visited != 0) {
      // Find min distance vertex in unvisited vertices. Choose that to be currentV
      currentV.distance = std::numeric_limits<int>::max();
      for (size_t i = 0; i < nodeList.size(); ++i) {
        if ((nodeList[i].distance < currentV.distance) && !nodeList[i].visited) {
          currentV = nodeList[i];
        }
      }
    }
    // iterate over all vertices
    for (size_t i = 0; i < nodeList.size(); ++i) {
      int edgeWeight = g.getEdge(currentV.nodeNum,i);
      if (edgeWeight == 0) {
        // 0 in matrix means this node is not adjacent to currentV
        continue;
      }
      int altPathDis;
      // If the distance is int_max, then any edgeWeight will be shorter. (avoid int overflow)
      if (nodeList[currentV.nodeNum].distance == std::numeric_limits<int>::max()) {
        altPathDis = edgeWeight;
      } else {
        altPathDis = nodeList[currentV.nodeNum].distance + edgeWeight;
      }
      // Update fromNode and distance when altPathDis is shorter than past record of distance.
      if (altPathDis < nodeList[i].distance) {
        nodeList[i].distance = altPathDis;
        nodeList[i].fromNode = currentV.nodeNum;
      } 
    }
    // Set currentV to visited
    nodeList[currentV.nodeNum].visited = true;
    visited++;

    // Print current info of nodes.
    std::cout << "visited node " << currentV.nodeNum << std::endl;
        for (size_t i = 0; i < nodeList.size(); ++i) {
      std::cout << "nodeNum: " << nodeList[i].nodeNum << std::endl;
      std::cout << "distance: " << nodeList[i].distance;
      std::cout << ", fromNode: " << nodeList[i].fromNode;
      std::cout << ", visited: " << (nodeList[i].visited ? "true" : "false") << std::endl;
    }
    std::cout << std::endl;
  }
  return nodeList;
}

// From the result of allShortestPath, trace out the path to desired end vertex
std::vector<size_t> shortestPathToEnd(const std::vector<NodeForDijkstra>& allShortestPaths, const size_t end) {
  std::vector<size_t> vec;
  size_t node = end;
  while (node != std::numeric_limits<size_t>::max()) {
    vec.push_back(node);
    // The next node will be "fromNode" of current node.
    node = allShortestPaths[node].fromNode;
  }
  
  // Reverse the vector, so that 0th index is the start vertex and last index is the
  // end vertex
  for (size_t index = 0; index < vec.size()/2; ++index) {
    std::swap(vec[index], vec[vec.size()-1-index]);
  }

  return vec;
}

int main() {
  // Prepare a matrix of edge weight to construct a graph. This becomes the adjacency matrix 
  std::vector<std::vector<int>> sample = { {0, 2, 5, 0, 1}, 
                                           {2, 0, 3, 0, 2},
                                           {5, 3, 0, 7, 0},
                                           {0, 0, 7, 0, 1},
                                           {1, 2, 0, 1, 0} };
  Graph g(sample);
  std::cout << "Find the shortest paths from vertex 4 to all other vertices" << std::endl;
  std::vector<NodeForDijkstra> fromNode4 = allShortestPath(g, 4);

  // With all the shortest paths, trace path to vertex 2 
  std::vector<size_t> thePath = shortestPathToEnd(fromNode4, 2);
  std::cout << "The Path: " << std::endl;
  for (size_t i = 0; i < thePath.size(); ++i) {
    std::cout << thePath[i] << " ";
  }
  std::cout << std::endl;
  return 0;
}