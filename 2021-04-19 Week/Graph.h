/*
Programmer: Alex Fung
Programmer's ID: 1873719
Class: COMPSC-210-5097
Date: 2021-04-24
Programming Environment Used: Linux, gcc 8.3
*/
#ifndef GRAPH_H
#define GRAPH_H

#include <iostream>
#include <limits>
#include <vector>

class Graph {
  public:
    Graph(size_t);
    Graph(const std::vector<std::vector<int>>&);
    void validateVertex(const size_t) const;
    void validateEdge(const int) const;
    void setEdge(const size_t, const size_t, int);
    int getEdge(const size_t, const size_t) const;
    std::vector<std::vector<int>> getMatrix() const;
    size_t verticesCount() const;
  private:
    std::vector<std::vector<int>> matrix;
};
#endif