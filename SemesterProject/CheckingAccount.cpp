/*
Programmer: Alex Fung
Programmer's ID: 1873719
Class: COMPSC-210-5097
Date: 2021-05-07
Programming Environment Used: Linux, gcc 8.3
*/

#include "CheckingAccount.h"

CheckingAccount::CheckingAccount(double initialBalance, double fee)
  : Account(initialBalance), transactionFee(fee) {
}

bool CheckingAccount::debitPossible(double amount) const {
  double totalDebit = amount + transactionFee;
  return Account::debitPossible(totalDebit);
}

bool CheckingAccount::debit(double amount) {
  if (debitPossible(amount)) {
    double totalDebit = amount + transactionFee;
    if (!Account::debit(totalDebit)) {
      std::cout << "Error: Debit Incomplete. Contact Support." << std::endl;
      return false;
    }
    std::cout << "$" << transactionFee << " checking account transaction fee charged." << std::endl;
    return true;
  }
  return false;
}

bool CheckingAccount::creditPossible(double amount) const {
  double totalCredit = amount - transactionFee;
  return Account::creditPossible(totalCredit);
}

bool CheckingAccount::credit(double amount) {
  if (creditPossible(amount)) {
    double totalCredit = amount - transactionFee;
    if (!Account::credit(totalCredit)) {
      std::cout << "Error: Credit Incomplete. Contact Support." << std::endl;
      return false;
    }
    std::cout << "$" << transactionFee << " checking account transaction fee charged." << std::endl;
    return true;
  }
  return false;
}

double CheckingAccount::getTransactionFee() const {
  return transactionFee;
}

std::ostream& operator<<(std::ostream& output, const CheckingAccount& c) {
  output << c.getBalance();
  return output;
}

bool CheckingAccount::operator+=(double amount) {
  return credit(amount);
}

bool CheckingAccount::operator-=(double amount) {
  return debit(amount);
}

CheckingAccount CheckingAccount::operator+(double amount) const {
  CheckingAccount newAccount{getBalance(), getTransactionFee()};
  newAccount.credit(amount);
  return newAccount;
}

CheckingAccount operator+(double amount, const CheckingAccount& a) {
  return a.operator+(amount);
}

CheckingAccount CheckingAccount::operator-(double amount) const {
  CheckingAccount newAccount{getBalance(), getTransactionFee()};
  newAccount.debit(amount);
  return newAccount;
}

// Assignment operator copies the data from other object to this object.
void CheckingAccount::operator=(const CheckingAccount& other) {
  balance = other.getBalance();
  transactionFee = other.getTransactionFee();
}
