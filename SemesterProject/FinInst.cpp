/*
Programmer: Alex Fung
Programmer's ID: 1873719
Class: COMPSC-210-5097
Date: 2021-05-07
Programming Environment Used: Linux, gcc 8.3
*/

#include "FinInst.h"

// Constructor using default value
FinInst::FinInst() {
}

// Constructor to specify all initial member variables
FinInst::FinInst(double transferFeePerc, double transferFeeM, double transF, double bal) 
  : transferFeePercentage(transferFeePerc), transferFeeMin(transferFeeM), 
    transactionFee(transF), balance(bal) 
{
}

bool FinInst::transfer(double amount, Account* sourceAccount, Account* destinationAccount) {
  if (amount < 0) {
    std::cout << "$" << amount << " must be positive. Exiting" << std::endl;
    return false;
  }
  // First, try to collect the transactionFee. If unsucessful, exit.
  if (!sourceAccount->debit(transactionFee)) {
    std::cout << "Transaction Fee could not be debited. Exiting. " << std::endl;
    return false;
  }

  balance += transactionFee;
  // Then, calculate transferFee
  double transferFee = amount * transferFeePercentage;
  if (transferFee < transferFeeMin) {
    transferFee = transferFeeMin;
  }
  // Then, try collecting transferFee. If it was unsuccessful, exit.
  if (!sourceAccount->debit(transferFee)) {
    std::cout << "Transfer Fee could not be debited. Exiting. " << std::endl;
    return false;
  }
  balance += transferFee;
  // Then, take the money out of source account. If it was unsuccessful, exit
  if (!sourceAccount->debit(amount)) {
    return false;
  }
  // Finally, attempt to credit the destination account. If It was unsuccessful, 
  // attempt to credit the sourceAccount
  if (!destinationAccount->credit(amount)) {
    // credit was unsuccessful, attempt to return amount to sourceAccount
    sourceAccount->credit(amount);
    return false;
  }
  return true;
  
}

double FinInst::getBalance() const {
  return balance;
}

std::ostream& operator<<(std::ostream& stream, const FinInst& finInst) {
  stream << finInst.getBalance();
  return stream;
}
