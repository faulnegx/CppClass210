/*
Programmer: Alex Fung
Programmer's ID: 1873719
Class: COMPSC-210-5097
Date: 2021-05-07
Programming Environment Used: Linux, gcc 8.3
*/

#ifndef ACCOUNT_H
#define ACCOUNT_H

#include <iostream>

class Account {
  public:
    Account(double = 0);
    double getBalance() const;
    virtual bool debitPossible(double) const;
    virtual bool debit(double);
    virtual bool creditPossible(double) const;
    virtual bool credit(double);
    friend std::ostream& operator<<(std::ostream&, const Account&);
    virtual bool operator+=(double);
    virtual bool operator-=(double);
    Account operator+(double) const;
    friend Account operator+(double, const Account&);
    Account operator-(double) const;
    void operator=(const Account&);
  // Comment 1: balance is protected to allow child classes to modify it directly
  protected:
    double balance;
};

#endif
