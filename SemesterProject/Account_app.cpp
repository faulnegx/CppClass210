/*
Programmer: Alex Fung
Programmer's ID: 1873719
Class: COMPSC-210-5097
Date: 2021-05-07
Programming Environment Used: Linux, gcc 8.3
*/

#include <iostream>
#include <iomanip>
#include "Account.h"
#include "SavingsAccount.h"
#include "CheckingAccount.h"
#include "FinInst.h"
using namespace std;

// This is the application file. 
int main()
{
  SavingsAccount savings1( 47.0, 0.1 );
  CheckingAccount checking1( 65.0, 1.0 );
  // Set precision to show money in only 2 decimal.
  cout << fixed << setprecision( 2 );

  cout << "Initial Savings1 account balance: $" << savings1 << endl;
  cout << "Initial Checking1 account balance: $" << checking1 << endl;
  cout << endl;

  savings1+=51.0;
  checking1+=77.0;

  cout << "After adding $51, Savings1 balance: $" << savings1 << endl;  
  cout << "After adding $77, Checking1 balance: $" << checking1 << endl;

  savings1-=51.0;
  checking1-=77.0;
   
  cout << "After subtracting $51, Savings1 balance: $" << savings1 << endl;
  cout << "After subtracting $77, Checking1 balance: $" << checking1 << endl;
  cout << endl;

  SavingsAccount savings2{100, 0.01};
  cout << "New Savings Account Savings2 balance: $" << savings2 << endl;
  savings2 = savings2 + 4.0;
  cout << "After adding $4 to Savings2, balance: $" << savings2 << endl;
  savings2 = 3.0 + savings2;
  cout << "After adding Savings2 to $3, balance: $" << savings2 << endl;
  savings2 = savings2 - 2.0;
  cout << "After subtracting $2, balance: $" << savings2 << endl;

  cout << endl;
  cout << "Prior to earning interest, Savings2 balance is $ " << savings2 << endl;
  cout << "Savings2 interest rate is " << savings2.getInterestRate() << endl;
  for (int i = 0; i < 10; ++i) {
    cout << "Savings2 will earn $" << savings2.calculateInterest() << " this period" << endl;
    savings2.earnInterest();
    cout << "Savings2 balance is now " << savings2 << endl;
  }
  cout << endl;

  CheckingAccount checking2{75, 2};
  cout << "New Checking Account Checking2 balance $" << checking2 << endl;
  checking2 = checking2 + 5.0;
  cout << "After adding $5 to Checking Account, balance: $" << checking2 << endl;
  checking2 = 6.0 + checking2;
  cout << "After adding Checking Account to $6, balance: $" << checking2 << endl;
  checking2 = checking2 - 4;
  cout << "After subtracting $4, balance: $" << checking2 << endl;

  checking2 = checking1;
  cout << "Afer assigning Checking1 to Checking2, both balances are $" << checking2 << endl;
  cout << endl;

  // Here we are finally creating a financial institution. 
  FinInst WestUnion{};
  Account* checking1Ptr = static_cast<Account*>(&checking1);
  Account* checking2Ptr = static_cast<Account*>(&checking2);
  int roundTripCount = 0;
  cout << "WestUnion will help assist with transaction back and forth between";
  cout << " 2 checking acounts. " << endl;
  while (WestUnion.transfer(40.0, checking1Ptr, checking2Ptr) && WestUnion.transfer(40.0, checking2Ptr, checking1Ptr)) {
    std::cout << "Transfer Back and Forth succeeded!" << std::endl;
    ++roundTripCount;
  } 
  cout << endl;

  std::cout << "After " << roundTripCount << " roundtrips, ";
  std::cout << "transfer failed. Account may have been debited anyway. ";
  std::cout << "Sorry for the inconvenience. " << std::endl;
  cout << "Final Checking1 balance $" << checking1 << endl;
  cout << "Final Checking2 balance $" << checking2 << endl;
  cout << endl;
  cout << "WestUnion's balance is now $" << WestUnion << ". After only ";
  cout << roundTripCount << " roundtrips. " << endl;

  return 0;
   
}
