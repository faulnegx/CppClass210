/*
Programmer: Alex Fung
Programmer's ID: 1873719
Class: COMPSC-210-5097
Date: 2021-05-07
Programming Environment Used: Linux, gcc 8.3
*/

#include "Account.h"

Account::Account(double initialBalance): balance(initialBalance) {
  if (initialBalance < 0) {
    balance = 0;
    std::cout << "Initial balance must be greater than or equal to 0.0. ";
    std::cout << "Initial balance set to 0.0. " << std::endl;
  }
}

double Account::getBalance() const {
  return balance;
}

bool Account::debitPossible(double amount) const {
  if (balance < amount) {
    std::cout << "Debit amount exceeded account balance" << std::endl;
    return false;
  }
  return true;
}

bool Account::debit(double amount) {
  if (debitPossible(amount)) {
    balance -= amount;
    return true;
  }
  return false;
}

bool Account::creditPossible(double amount) const {
  if (amount < 0) {
    std::cout << "Cannot add negative credit to balance." << std::endl;
    return false;
  }
  return true;
}

bool Account::credit(double amount) {
  // return true if successful
  if (creditPossible(amount)) {
    balance += amount;
    return true;
  }
  return false;
}

std::ostream& operator<<(std::ostream& output, const Account& a) {
  // This output operator overload makes it easier to print account
  // balance.
  output <<  a.getBalance();
  return output;
}

bool Account::operator+=(double amount) {
  // += and -= use the credit and debit function internal to the class
  return credit(amount);
}

bool Account::operator-=(double amount) {
  return debit(amount);
}

Account Account::operator+(double amount) const {
  // Binary operator +/- requires the creation of a brand new object.
  Account newAccount{getBalance()};
  newAccount.credit(amount);
  return newAccount;
}

Account operator+(double amount, const Account& a) {
  // friend plus operator does the same thing as the reverse order.
  return a.operator+(amount);
}

Account Account::operator-(double amount) const {
  Account newAccount{getBalance()};
  newAccount.debit(amount);
  return newAccount;
}

void Account::operator=(const Account& other) {
  // Assignment operator copies data form other object to this object.
  balance = other.getBalance();
}
