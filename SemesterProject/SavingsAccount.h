/*
Programmer: Alex Fung
Programmer's ID: 1873719
Class: COMPSC-210-5097
Date: 2021-05-07
Programming Environment Used: Linux, gcc 8.3
*/

#ifndef SAVINGSACCOUNT_H
#define SAVINGSACCOUNT_H

#include "Account.h"

class SavingsAccount : public Account {
  public:
    // Default balance and interestRate is set.
    SavingsAccount(double = 0, double = 0.01);
    double calculateInterest() const;
    double getInterestRate() const;
    void earnInterest();
    friend std::ostream& operator<<(std::ostream&, const SavingsAccount&);
    bool operator+=(double);
    bool operator-=(double);
    SavingsAccount operator+(double) const;
    friend SavingsAccount operator+(double, const SavingsAccount&);
    SavingsAccount operator-(double) const;
    void operator=(const SavingsAccount&);
  private:
    double interestRate;
};

#endif
