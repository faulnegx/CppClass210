/*
Programmer: Alex Fung
Programmer's ID: 1873719
Class: COMPSC-210-5097
Date: 2021-05-07
Programming Environment Used: Linux, gcc 8.3
*/

#ifndef CHECKINGACCOUNT_H
#define CHECKINGACCOUNT_H

#include "Account.h"

class CheckingAccount : public Account {
  public:
    CheckingAccount(double = 0, double = 1);
    bool debitPossible(double) const override;
    bool debit(double) override;
    bool creditPossible(double) const override;
    bool credit(double) override;
    double getTransactionFee() const;
    friend std::ostream& operator<<(std::ostream&, const CheckingAccount&);
    bool operator+=(double) override;
    bool operator-=(double) override;
    CheckingAccount operator+(double) const;
    friend CheckingAccount operator+(double, const CheckingAccount&);
    CheckingAccount operator-(double) const;
    void operator=(const CheckingAccount&);
  private:
    double transactionFee;
};


#endif
