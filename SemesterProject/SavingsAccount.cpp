/*
Programmer: Alex Fung
Programmer's ID: 1873719
Class: COMPSC-210-5097
Date: 2021-05-07
Programming Environment Used: Linux, gcc 8.3
*/

#include "SavingsAccount.h"

SavingsAccount::SavingsAccount(double initialBalance, double rate)
  : Account(initialBalance), interestRate(rate) {
}

double SavingsAccount::calculateInterest() const {
  return getBalance() * interestRate;
}

double SavingsAccount::getInterestRate() const {
  return interestRate;
}

void SavingsAccount::earnInterest() {
  credit(calculateInterest());
}

std::ostream& operator<<(std::ostream& output, const SavingsAccount& s) {
  output << s.getBalance();
  return output;
}

bool SavingsAccount::operator+=(double amount) {
  return credit(amount);
}

bool SavingsAccount::operator-=(double amount) {
  return debit(amount);
}

SavingsAccount SavingsAccount::operator+(double amount) const {
  SavingsAccount newAccount{getBalance(), getInterestRate()};
  newAccount.credit(amount);
  return newAccount;
}

SavingsAccount operator+(double amount, const SavingsAccount& a) {
  return a.operator+(amount);
}

SavingsAccount SavingsAccount::operator-(double amount) const {
  SavingsAccount newAccount{getBalance(), getInterestRate()};
  newAccount.debit(amount);
  return newAccount;
}

void SavingsAccount::operator=(const SavingsAccount& other) {
  balance = other.getBalance();
  interestRate = other.getInterestRate();
}
