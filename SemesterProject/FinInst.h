/*
Programmer: Alex Fung
Programmer's ID: 1873719
Class: COMPSC-210-5097
Date: 2021-05-07
Programming Environment Used: Linux, gcc 8.3
*/

#include "Account.h"

// Fin Inst stands for Financial Institution
class FinInst {
  public:
    FinInst();
    // Second Constructor for specifying fees and initial balance
    FinInst(double, double, double, double);
    bool transfer(double, Account*, Account*);
    double getBalance() const;
    friend std::ostream& operator<<(std::ostream&, const FinInst&);
  private:
    double transferFeePercentage{0.01};
    double transferFeeMin{1.0};
    double transactionFee{5.0};
    double balance{0.0};
};
