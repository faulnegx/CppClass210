/*
Programmer: Alex Fung
Programmer's ID: 1873719
Class: COMPSC-210-5097
Date: 2021-03-08
Programming Environment Used: Linux, gcc 8.3
*/

#include <iostream>
#include <vector>
#include <climits> // INT_MAX

struct Node {
  Node(int val=INT_MAX) : value{val}{
  }
  Node* left{nullptr};
  Node* right{nullptr};
  int value;
};

struct Tree {
  Node* root{nullptr};
};

void PrintBSTPreOrder(const Node* const);


void BSTInsert(Tree& tree, int val) {
  if (tree.root == nullptr) {
    // Beware of memory leak.
    tree.root = new Node{val};
  } else {
    Node* currentNodePtr = tree.root;
    while (currentNodePtr != nullptr) {
      if (val < currentNodePtr->value) {
        if (currentNodePtr->left == nullptr) {
          // Beware of memory leak.
          currentNodePtr->left = new Node{val};
          currentNodePtr = nullptr;
        } else {
          currentNodePtr = currentNodePtr->left;
        }
      } else {
        // val >= currentNodePtr->value
        if (currentNodePtr->right == nullptr) {
          // Beware of memory leak.
          currentNodePtr->right = new Node{val};
          currentNodePtr = nullptr;
        } else {
          currentNodePtr = currentNodePtr->right;
        }
      }
    }
  }
}

Node* BSTSearch(Tree& tree, int val) {
  Node* currentNodePtr = tree.root;
  while (currentNodePtr != nullptr) {
    if (val == currentNodePtr->value) {
      return currentNodePtr;
    } else if (val < currentNodePtr->value) {
      currentNodePtr = currentNodePtr->left;
    } else {
      // val >= currentNodePtr->value
      currentNodePtr = currentNodePtr->right;
    }
  }
  return nullptr;
}


void PrintBSTInOrder(const Node* const nodeptr) {
  // Print left subtree nodes first, then print this node's value, 
  // then print right subtree nodes
  if (nodeptr == nullptr) {
    return;
  }
  PrintBSTInOrder(nodeptr->left);
  std::cout << nodeptr->value << " ";
  PrintBSTInOrder(nodeptr->right);
}

void PrintBSTPreOrder(const Node* const nodeptr) {
  // Print this node's value, then left subtree nodes first,
  // then print right subtree nodes
  if (nodeptr == nullptr) {
    return;
  }
  std::cout << nodeptr->value << " ";
  PrintBSTPreOrder(nodeptr->left);
  PrintBSTPreOrder(nodeptr->right);
}

int main() {
  std::vector<int> set{8, 4, 2, 5, 12, 29};
  Tree tree;
  // Insert all the item in the set to our BST
  for (int& ele : set) {
    BSTInsert(tree, ele);
  }
  PrintBSTPreOrder(tree.root);
  std::cout << std::endl;
  return 0;
}
