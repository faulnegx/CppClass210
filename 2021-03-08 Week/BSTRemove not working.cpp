void BSTRemove(Tree& tree, int val) {
  Node* parent = nullptr;
  Node* currentNodePtr = tree.root;
  while (currentNodePtr != nullptr) {
    if (currentNodePtr->value == val) {
      // Found our node
      PrintBSTPreOrder(tree.root);
      std::cout << std::endl;

      if (currentNodePtr->left == nullptr && currentNodePtr->right == nullptr) {
        // currentNode is a leaf node
        if (parent == nullptr) {
          // currentNode is root
          tree.root = nullptr;
        } else if (parent->left == currentNodePtr) {
          parent->left = nullptr;
        } else {
          parent->right = nullptr;
        }
      } else if (currentNodePtr->right == nullptr) {
        // current node has no right child. 
        if (parent == nullptr) {
          // currentNode is root
          tree.root = currentNodePtr->left;
        } else if (parent->left == currentNodePtr) {
          parent->left = currentNodePtr->left;
        } else {
          parent->right = currentNodePtr->left;
        }
      } else if (currentNodePtr->left == nullptr) {
        // current node has no left child. 
        if (parent == nullptr) {
          // currentNode is root
          tree.root = currentNodePtr->right;
        } else if (parent->left == currentNodePtr) {
          parent->left = currentNodePtr->right;
        } else {
          parent->right = currentNodePtr->right;
        }
      } else {
        // current node has both left and right children
        // successor to be the left most child of the right subtree
        Node* sucessorNodePtr = currentNodePtr->right;
        while (sucessorNodePtr->left != nullptr) {
          sucessorNodePtr = sucessorNodePtr->left;
        }
        // Save a temporary copy of the value
        int successorValue = sucessorNodePtr->value;
        std::cout << successorValue << std::endl;
        BSTRemove(tree, sucessorNodePtr->value);
        // value must be set after the successor node is deleted 
        // (otherwise there will be 2 nodes with the same value in the tree.)
        currentNodePtr->value = successorValue;        
      }
    } else if (currentNodePtr->value < val) {
      // Search right subtree
      parent = currentNodePtr;
      currentNodePtr = currentNodePtr->right;
    } else {
      // Search left subtree
      parent = currentNodePtr;
      currentNodePtr = currentNodePtr->left;
    }
  }
  return; // Search through tree and no node matching val found.
}
