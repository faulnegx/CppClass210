/*
Programmer: Alex Fung
Programmer's ID: 1873719
Class: COMPSC-210-5097
Date: 2021-04-11
Programming Environment Used: Linux, gcc 8.3
*/

#include "Command.h"

Command::Command(int prio, int sp, std::string cmdtype, std::string zn)
  : priority{prio}, setpoint{sp}, commandType{cmdtype}, zoneName{zn} {
}
// Overload the comparison operators for priority_queue's usage. All comparison are made with the member variable priority
bool Command::operator>(const Command& other) const {
  return priority>other.priority;
}
bool Command::operator<(const Command& other) const {
  return priority<other.priority;
}
bool Command::operator>=(const Command& other) const {
  return priority>=other.priority;
}
bool Command::operator<=(const Command& other) const {
  return priority<=other.priority;
}
bool Command::operator==(const Command& other) const {
  return priority==other.priority;
}
bool Command::operator!=(const Command& other) const {
  return priority!=other.priority;
}
std::string Command::toString() const{
  return "Zone: " + zoneName
       + "\nPriority: " + std::to_string(priority)
       + "\nSetpoint: " + std::to_string(setpoint)
       + "\nCommand Type: " + commandType;
}
