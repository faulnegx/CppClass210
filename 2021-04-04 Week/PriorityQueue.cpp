/*
Programmer: Alex Fung
Programmer's ID: 1873719
Class: COMPSC-210-5097
Date: 2021-04-11
Programming Environment Used: Linux, gcc 8.3
*/

#include <iostream>
#include <queue>

#include "Command.h"

int main() {
  const std::string Intro =  "\nIn an office building, there is a smart building server "
                             "that process all smart building service. This server "
                             "execute commands in order of priority (higher number is "
                             "higher priority).";
  std::cout << Intro << std::endl;
  const std::string FirstStep = "The program first load in various command including "
                                "lightswitch(which user expects near instant response), "
                                "Thermostat(which user expects a slower response), "
                                "and Machine Learning process(which the user is not "
                                "interfacing with at all.";
  std::cout << FirstStep << "\n\n" << std::endl;
  // Create STL's priority_queue as a commandQueue for the smart building server.
  std::priority_queue<Command> commandQueue{};
  // Note that low priority tasks are enqueued first.
  // priority_queue's push function is equivalent to Enqueue
  Command a{1, -1, "Machine learning processes", "Private office 4"};
  Command b{50, 100, "Lightswitch", "Conference room 1"};
  Command c{30, 80, "Thermostat", "Main Lobby"};
  Command d{50, 0, "Lightswitch", "Conference room 2"};
  Command e{30, 60, "Thermostat", "Private office 5"};
  commandQueue.push(a);
  commandQueue.push(b);
  commandQueue.push(c);
  commandQueue.push(d);
  commandQueue.push(e);
  // Continue dequeuing priority only if it is not empty.
  // priority_queue's empty function is equivalent to IsEmpty
  while (!commandQueue.empty()) {
    std::cout << "------------" << std::endl;
    // priority_queue's size function is equivalent to GetLength
    std::cout << "Command Queue size: " << commandQueue.size() << std::endl;
    std::cout << "Processing: " << std::endl;
    // priority_queue's top function is equivalent to Peek
    // Note that Lightswitch command is at the top of the queue even though Machine
    // learning processes is enqueued first. This is because Lightswitch command has 
    // higher priority.
    std::cout << commandQueue.top().toString() << std::endl;
    // Dequeue top element of Command Queue after accessing the top element
    // priority_queue's pop function is equivalent to Dequeue
    commandQueue.pop();
    std::cout << std::endl;
  }
  
  std::cout << "------------" << std::endl;
  std::cout << "Command Queue is now empty." << std::endl;
  std::cout << "Command Queue size: " << commandQueue.size() << std::endl;
  std::cout << "All commands have been executed in order of their priority" << std::endl;

  return 0;
}

