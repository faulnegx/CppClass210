
/*
Programmer: Alex Fung
Programmer's ID: 1873719
Class: COMPSC-210-5097
Date: 2021-04-11
Programming Environment Used: Linux, gcc 8.3
*/

#ifndef COMMAND_H
#define COMMAND_H

#include <iostream>
#include <string>

class Command{
  public:
    Command(int, int, std::string, std::string);
    bool operator>(const Command&) const;
    bool operator<(const Command&) const;
    bool operator>=(const Command&) const;
    bool operator<=(const Command&) const;
    bool operator==(const Command&) const;
    bool operator!=(const Command&) const;
    std::string toString() const;

    int priority;
    int setpoint;
    std::string commandType;
    std::string zoneName;
};

#endif