/*
Programmer: Alex Fung
Programmer's ID: 1873719
Class: COMPSC-210-
Date: 2021-02-07
Programming Environment Used: Linux, gcc 8.3
*/
#include <iostream>
#include <limits> // std::numeric_limits
#include <vector>

struct IntNode {
  IntNode() = default;
  IntNode(int value=0, IntNode* ptr=nullptr): val(value), next(ptr) {};
  int val;
  IntNode* next;
};

class LinkedList {
  public: 
    LinkedList() : head{nullptr}, m_size{0} {
    }
    LinkedList(const IntNode& initNode) : head{nullptr}, m_size{1} {
      head = new IntNode{initNode.val, initNode.next};
    }

    // Rule of three - Copy constructor
    LinkedList(const LinkedList& incomingList) : head{nullptr}, m_size{0} {
      // Make new LinkedList. Deep copying all the nodes
      if (incomingList.size() > 0) {
        head = new IntNode{incomingList.getVal(0)};
        ++m_size;
        IntNode* current{head};
        for (size_t i = 1; i < incomingList.size(); ++i) {
          current->next = new IntNode{incomingList.getVal(i)};
          ++m_size;
          current = current->next;
        }
      }
    }

    // Rule of three - Copy assignment operator
    LinkedList& operator=(const LinkedList& incomingList) {
      if (this == &incomingList) {
        return *this;
      } else {
        // delete all the nodes in this list, there is potential to optimize this to not
        // delete all the nodes and just replace the values
        while(this->size() > 0) {
          this->deleteNode(0);
        }
        // Potential to optimize to step through both incomingList and this List together
        // But we are not using LinkedList's head pointer
        for(size_t i = 0;i < incomingList.size(); ++i) {
          if (i == 0) {
            this->pushFront(incomingList.getVal(i));
          } else {
            this->insertNode(i, incomingList.getVal(i));
          }
        }
        return *this;
      }
    }

    void pushFront(const IntNode& newNode) {
      // We are disregarding the node's original next pointer.
      this->pushFront(newNode.val);
    }

    void pushFront(const int newVal) {
      // Make new node from newVal. New node's next is head.
      head = new IntNode{newVal, head};
      ++m_size;
    }

    void insertNode(const size_t index, const int newVal) {
      if (this->size() == index) {
        IntNode* current{head};
        for (size_t i=0;i < index-1; ++i) {
          current = current->next;
        }
        current->next = new IntNode{newVal, current->next};
        ++m_size;
      } else if (index >= this->size()) {
        std::cout << "LinkedList.insertNode(): index beyond end of list" << std::endl;
      } else if (index == 0) {
        this->pushFront(newVal);
      } else {
        IntNode* current{head};
        // Travel to index
        for (size_t i=0;i < index-1; ++i) {
          current = current->next;
        }
        current->next = new IntNode{newVal, current->next};
        ++m_size;
      }     
    }

    size_t searchNode(const int searchVal) const {
      IntNode* current{head};
      size_t index = 0;
      while (current != nullptr) {
        if (current->val == searchVal) {
          return index;
        }
        current = current->next;
        ++index;
      }
      // return size_t max when searchVal is not found
      return std::numeric_limits<size_t>::max();
    }

    void setVal(const size_t index, int newVal) {
      if (index >= this->size()) {
        // index beyond end of list
        std::cout << "LinkedList.setVal(): index beyond end of list" << std::endl;
      } else {
        IntNode* current{head};
        for (size_t i=0;i < index; ++i) {
          current = current->next;
        }
        current->val = newVal;
      }
    }
    int getVal(const size_t index) const {
      if (index >= this->size()) {
        // index beyond end of list
        std::cout << "LinkedList.getVal(): index beyond end of list" << std::endl;
        return std::numeric_limits<int>::max();
      } else {
        IntNode* tempPointer{head};
        for (size_t i=0;i < index; ++i) {
          tempPointer = tempPointer->next;
        }
        return tempPointer->val;
      }
    }

    void deleteNode(const size_t index) {
      if (this->size() == 0) {
        std::cout << "LinkedList.deleteNode(): list is empty" << std::endl;
      } else if (index >= this->size()) {
        // index beyond end of list
        std::cout << "LinkedList.deleteNode(): index beyond end of list" << std::endl;
      } else {
        if (index == 0) {
          IntNode* tempPointer{head};
          head = head->next;
          delete tempPointer;
        } else {
          IntNode* nodeBeforeIndexPtr{head};
          for (size_t i=0;i < index-1; ++i) {
            nodeBeforeIndexPtr = nodeBeforeIndexPtr->next;
          }
          IntNode* nodeAtIndexPtr = nodeBeforeIndexPtr->next;
          nodeBeforeIndexPtr->next = nodeAtIndexPtr->next;
          delete nodeAtIndexPtr;
        }
        --m_size;
      }
    }

    size_t size() const {
      return m_size;
    }
    void print() const {
      IntNode* current{head};
      if (current != nullptr) {
        IntNode* nextNodePtr{current->next};
        while (nextNodePtr != nullptr) {
          std::cout << current->val << " ";
          current = nextNodePtr;
          nextNodePtr = current->next;
        }
        std::cout << current->val;
      }
      std::cout << std::endl;
    }

    // Rule of three - Destructor
    ~LinkedList() {
      IntNode* current{head};
      if (current != nullptr) {
        // list is not empty
        IntNode* nextNodePtr{(*current).next};
        while (nextNodePtr != nullptr) {
          delete current;
          current = nextNodePtr;
          nextNodePtr = current->next;
        }
        delete current;
      }
    }

  protected: 
    IntNode* head;
  private:
    size_t m_size;
};


class SortedList : public LinkedList {
  public:
    SortedList(const std::vector<int>& vec){
      if (vec.size() > 0) {
        this->pushFront(vec.at(0));
        for (size_t i = 1; i < vec.size(); ++i) {
          insert(vec.at(i));
        }
      }
    }

    size_t linearSearchNodeGreaterEqual(const int searchVal) const {
      // Return the index where val is >= searchVal
      IntNode* current{head};
      size_t index = 0;
      while (current != nullptr) {
        if (current->val >= searchVal) {
          return index;
        }
        current = current->next;
        ++index;
      }
      // return size_t max when searchVal is not found
      return std::numeric_limits<size_t>::max();
    }

    void insert(const int newVal) {
      if (this->size() == 0) {
        this->pushFront(newVal);
      } else {
        size_t indexToInsert = linearSearchNodeGreaterEqual(newVal);
        if (indexToInsert == std::numeric_limits<size_t>::max()) {
          indexToInsert = this->size();
        }
        insertNode(indexToInsert, newVal);
      }
    }
};

int main() {
  std::cout << "\nSample program illustrating LinkedList implemented in class \n" << std::endl;

  LinkedList sampleLinkedList1{};
  std::cout << "SampleLinkedList1 contents(empty list): ";
  sampleLinkedList1.print();
  IntNode node1{3};
  sampleLinkedList1.pushFront(node1);
  std::cout << "SampleLinkedList1 contents after pushFront from IntNode: ";
  sampleLinkedList1.print();
  sampleLinkedList1.pushFront(-5);
  std::cout << "SampleLinkedList1 contents after pushFront from int: ";
  sampleLinkedList1.print();
  std::cout << "size of SampleLinkedList1 is " << sampleLinkedList1.size() << std::endl;
  
  LinkedList sampleLinkedList2{sampleLinkedList1};
  std::cout << "SampleLinkedList2 contents(created from SampleLinkedList1): ";
  sampleLinkedList2.print();
  sampleLinkedList2.deleteNode(1);
  std::cout << "SampleLinkedList2 contents after deleted Node at index 1: ";
  sampleLinkedList2.print();

  std::cout << "\nProblem: we wish to maintain a list with sorted rent prices\n" << std::endl;

  std::cout << "Solution: Inheriting from LinkedList, we can easily create such a sorted list. " << std::endl;
  std::cout << "Using linked list can be more efficient because we will not have to " << std::endl;
  std::cout << "reallocate memory when new apartment prices are to be inserted in to our sorted list.\n" << std::endl;

  std::cout << "unsorted prices: ";
  std::vector<int> aptPrices{1234, 1324, 1450, 1700, 1640};
  for (auto val : aptPrices) {
    std::cout << val << " ";
  }
  std::cout << std::endl;

  std::cout << "SortedPrices created from unsorted prices: ";
  SortedList SortedPrices{aptPrices};
  SortedPrices.print();

  SortedPrices.insert(2000);
  std::cout << "SortedPrices remains sorted after 2000 is inserted: ";
  SortedPrices.print();
  SortedPrices.insert(1400);
  std::cout << "SortedPrices remains sorted after 1400 is inserted: ";
  SortedPrices.print();
  SortedPrices.insert(1000);
  std::cout << "SortedPrices remains sorted after 1000 is inserted: ";
  SortedPrices.print();

  std::cout << "SortedPrices remains sorted after 1324 is removed: ";
  SortedPrices.deleteNode(SortedPrices.searchNode(1324));
  SortedPrices.print();
  std::cout << std::endl;

  return 0;
}
